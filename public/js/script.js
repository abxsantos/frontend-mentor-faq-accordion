const radios = document.querySelectorAll('.accordion-radio');

function handleClickEvent(event){
    displayAccordionDetails(event.target)
}

function displayAccordionDetails(target) {
    const details = target.parentElement.parentElement.querySelector('.accordion-details');
    if (details) {
        document.querySelectorAll('.accordion-details').forEach(detail => {
            detail.classList.remove('show');
        });
        details.classList.add('show');
    }
}

radios.forEach(radio => {
    radio.addEventListener('click', handleClickEvent);

    if (radio.checked){
        displayAccordionDetails(radio)
    }
});
